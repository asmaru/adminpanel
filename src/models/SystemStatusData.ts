export default interface SystemStatusData {

	usedSpace: number;

	resourceCount: number;

	version: string;

	buildDate: number;

	phpVersion: string;
}