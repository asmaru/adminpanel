/* eslint-disable */

export default class ResourceWriteRequest {
	public name!: string;
	public type!: string;
	public data!: any;
}