/* eslint-disable */
export default interface FileMetaData {

	alt: string;

	title: string;

	description: string;
}