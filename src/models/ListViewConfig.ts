export default class ListViewConfig {
	public sortByField: string = 'changed';
	public ascending: boolean = false;
	public grid: boolean = false;
}