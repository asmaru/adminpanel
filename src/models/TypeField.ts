/* eslint-disable */
export default // noinspection JSUnusedGlobalSymbols
interface TypeField {
	name: string;
	type: string;
	required: boolean;
	editor: string;
	label: string;
	hidden: boolean;
}