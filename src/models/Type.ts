/* eslint-disable */

export default interface Type {
	name: string;
	fields: object;
}