/* eslint-disable */

export default class ImageDimensionResponse {

	public height!: number;

	public width!: number;
}