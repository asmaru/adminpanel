import RequestData from '@/models/RequestData';

export default interface TrackingData {

	requests: RequestData[];
}