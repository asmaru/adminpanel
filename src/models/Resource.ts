/* eslint-disable */
import ImageDimensionResponse from '@/models/ImageDimensionResponse';

export default interface Resource {
	_self: string;

	_parent: string;

	_selfRelative: string;

	_parentRelative: string;

	_preview: string;

	_public: string;

	name: string;

	type: string;

	data: any;

	children: Resource[];

	size: number;

	changed: number;

	dimensions: ImageDimensionResponse | null;
}