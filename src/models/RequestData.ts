export default interface RequestData {

	protocol: string;

	path: string;

	userAgent: string;

	referer: string;

	dnt: string;

	time: string;

	status: string;

	acceptLanguage: string;

	accept: string;

	acceptEncoding: string;

	error: boolean;
}