/* eslint-disable */

export default class UploadDataItem {
	public name!: string;
	public file!: File;
	public dataUri!: string;
	public loading: boolean = true;
}