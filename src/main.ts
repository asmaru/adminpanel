import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './components/App.vue';
import CreateFolderForm from '@/components/dialog/CreateFolderForm.vue';
import CreateMediaForm from '@/components/dialog/CreateMediaForm.vue';
import Browser from '@/components/modules/Browser.vue';
import Stats from '@/components/modules/Stats.vue';
import Dashboard from '@/components/modules/Dashboard.vue';
import Login from '@/components/modules/Login.vue';
import State from '@/services/State';
import Backup from '@/components/modules/Backup.vue';
import EditorWrapper from '@/components/editor/EditorWrapper.vue';

Vue.config.productionTip = false;
Vue.use(VueRouter);

const routes = [
	{path: '/edit*', component: EditorWrapper, name: 'edit'},
	{path: '/createFolder*', component: CreateFolderForm, name: 'createFolder'},
	{path: '/createMedia*', component: CreateMediaForm, name: 'createMedia'},
	{path: '/dashboard', component: Dashboard, name: 'dashboard'},
	{path: '/browse*', component: Browser, name: 'browse'},
	{path: '/stats', component: Stats, name: 'stats'},
	{path: '/backup', component: Backup, name: 'backup'},
	{path: '/login', component: Login, name: 'login'}
];

const router = new VueRouter({
	routes: routes, // fullPath for `routes: routes`
	linkActiveClass: 'active',
	linkExactActiveClass: 'exact-active'
});

export const state = new State();

new Vue({
	router,
	render: h => h(App)
}).$mount('#app');