/* eslint-disable */

import axios from 'axios';
import Resource from '@/models/Resource';
import ResourceWriteRequest from '@/models/ResourceWriteRequest';
import Type from '@/models/Type';
import TrackingData from '@/models/TrackingData';
import ResourceService from '@/services/ResourceService';
import FileMetaData from '@/models/FileMetaData';
import {state} from '@/main';

export default class ApiService {

	private types: object = {};

	private auth: { password: string; username: string } = {password: '', username: ''};

	private user!: Resource;

	private apiPath: string = 'api/';

	private readonly cache: Map<string, Resource> = new Map();

	public getTypes() {
		return this.types;
	}

	public getType(resource: Resource): Type {
		let types: any = this.types;
		let typeName = resource.data.type;
		return types[typeName];
	}

	public loadTypes(): Promise<void> {
		let url: string = this.getPath('types');
		return new Promise((resolve, reject) => {
			axios.get(url, {auth: this.auth}).then((response) => {
				this.types = response.data;
				resolve();
			}, reject);
		});
	}

	public search(query: string, type: string, limit: number): Promise<Resource> {
		let path: string = '/?query=' + encodeURIComponent(query) + '&type=' + encodeURIComponent(type) + '&limit=' + limit + 'sortBy=changed';
		return this.getResource(this.getPath(path));
	}

	public getResourceByPath(path: string): Promise<Resource> {
		return this.getResource(this.getPath(path));
	}

	public getResource(url: string): Promise<Resource> {
		return new Promise((resolve, reject) => {
			if (this.cache.has(url)) {
				resolve(<Resource>this.cache.get(url));
				return;
			}
			axios.get(url, {auth: this.auth}).then((response) => {
				this.cache.set(url, response.data);
				resolve(<Resource>response.data);
			}, reject);
		});
	}

	public getStats(url: string): Promise<TrackingData> {
		return new Promise((resolve, reject) => {
			axios.get(this.getPath(url), {auth: this.auth}).then(function (response) {
				resolve(response.data);
			}, reject);
		});
	}

	public getBackups(): Promise<any> {
		return new Promise((resolve, reject) => {
			axios.get(this.getPath('backup'), {auth: this.auth}).then(function (response) {
				resolve(response.data);
			}, reject);
		});
	}

	public getBackup(name: string): Promise<any> {
		return new Promise((resolve, reject) => {
			axios.get(this.getPath('backup/' + name), {auth: this.auth}).then(function (response) {
				resolve(response.data);
			}, reject);
		});
	}

	public createBackup(): Promise<any> {
		return new Promise((resolve, reject) => {
			axios.post(this.getPath('backup'), null, {auth: this.auth}).then(function (response) {
				resolve(response.data);
			}, reject);
		});
	}

	public deleteBackup(name: string): Promise<void> {
		return new Promise((resolve, reject) => {
			axios.post(this.getPath('backup/' + name), null, {
				headers: {
					'x-http-method-override': 'DELETE'
				},
				auth: this.auth
			}).then(function () {
				resolve();
			}, reject);
		});
	}

	public getStatus(): Promise<any> {
		return new Promise((resolve, reject) => {
			axios.get(this.getPath('status'), {auth: this.auth}).then(function (response) {
				resolve(response.data);
			}, reject);
		});
	}

	public save(resource: Resource): Promise<Resource> {
		return new Promise((resolve, reject) => {
			axios.post(resource._self, resource, {
				headers: {
					'x-http-method-override': 'PUT'
				},
				auth: this.auth
			}).then((response) => {
				this.cache.set(resource._self, response.data);
				resolve(response.data);
			}, reject);
		});
	}

	public preview(resource: Resource): Promise<string> {
		return new Promise((resolve, reject) => {
			axios.post(this.getPath('preview' + resource._public), resource, {
				headers: {
					'x-http-method-override': 'PUT'
				},
				auth: this.auth
			}).then((response) => {
				resolve(response.data);
			}, reject);
		});
	}

	public flushCache(): Promise<void> {
		return new Promise((resolve, reject) => {
			axios.post(this.getPath('cache/all'), null, {
				headers: {
					'x-http-method-override': 'DELETE'
				},
				auth: this.auth
			}).then(() => {
				state.api.clearCache();
				resolve();
			}, reject);
		});
	}

	public getFileMetaData(resource: Resource): Promise<FileMetaData> {
		return new Promise((resolve, reject) => {
			let path = this.getPath('meta' + resource._selfRelative);
			axios.get(path, {auth: this.auth}).then(function (response) {
				resolve(response.data);
			}, reject);
		});
	}

	public saveFileMetaData(resource: Resource, meta: FileMetaData): Promise<Resource> {
		console.log('save meta', meta);
		return new Promise((resolve, reject) => {
			let path = this.getPath('meta' + resource._selfRelative);
			axios.post(path, meta, {
				headers: {
					'x-http-method-override': 'PUT'
				},
				auth: this.auth
			}).then((response) => {
				resolve(response.data);
			}, reject);
		});
	}

	public createFolder(name: string, parent: Resource): Promise<Resource> {
		let writeRequest = new ResourceWriteRequest();
		writeRequest.type = 'Folder';
		writeRequest.name = name;
		writeRequest.data = {};
		return this.createResource(writeRequest, parent);
	}

	public createPage(name: string, type: string, parent: Resource): Promise<Resource> {
		let data: any = {};
		let types: any = this.types;
		let typeDefinition = types[type];
		for (const fieldName in typeDefinition.fields) {
			data[fieldName] = null;
		}
		data.name = name;
		data.type = type;
		data.hidden = true;
		data.disabled = false;

		let writeRequest = new ResourceWriteRequest();
		writeRequest.type = 'Page';
		writeRequest.name = ResourceService.getInternalPath(name);
		writeRequest.data = data;

		return this.createResource(writeRequest, parent);
	}

	public createResource(resource: ResourceWriteRequest, parent: Resource): Promise<Resource> {
		let url: string = this.getPath(parent._selfRelative + '/' + this.slugify(resource.name) + '?noValidation');
		return new Promise((resolve, reject) => {
			axios.post(url, resource, {
				headers: {
					'x-http-method-override': 'PUT'
				},
				auth: this.auth
			}).then((response) => {
				state.api.clearCache();
				resolve(response.data);
			}, reject);
		});
	}


	public copy(resource: Resource, newValue: string): Promise<Resource> {
		let url: string = this.getPath(newValue + '?noValidation=1&source=' + encodeURI(resource._selfRelative));
		return new Promise((resolve, reject) => {
			axios.post(url, null, {
				headers: {
					'x-http-method-override': 'PUT'
				},
				auth: this.auth
			}).then((response) => {
				state.api.clearCache();
				resolve(response.data);
			}, reject);
		});
	}

	public deleteResource(resource: Resource): Promise<void> {
		return new Promise((resolve, reject) => {
			axios.post(resource._self, resource, {
				headers: {
					'x-http-method-override': 'DELETE'
				},
				auth: this.auth
			}).then(function () {
				state.api.clearCache();
				resolve();
			}, reject);
		});
	}

	public getPath(path: string) {
		if (path && path.startsWith('/')) {
			path = path.substr(1);
		}
		return ApiService.getBaseUrl() + this.apiPath + path;
	}

	public login(user: string, password: string): Promise<void> {
		this.auth = {
			username: user,
			password: password
		};
		return new Promise((resolve, reject) => {
			let path = this.getPath('/users/' + ResourceService.getInternalPath(user));
			axios.get(path, {
				auth: this.auth
			}).then((response) => {
				if (!response.data.name) {
					reject();
					return;
				}
				this.user = response.data;
				resolve();
			}, reject);
		});
	}

	public getCurrentUser(): Resource {
		return this.user;
	}

	public getPublicPath(resource: Resource): string {
		return ApiService.getBaseUrl().substring(0, ApiService.getBaseUrl().length - 1) + resource._public;
	}

	public slugify(text: string): string {
		text = text.toLowerCase();
		text = text.replace('ü', 'ue');
		text = text.replace('ö', 'oe');
		text = text.replace('ä', 'ae');
		text = text.replace('ß', 'ss');
		text = text.replace(/[^\w .\-_]+/g, '');
		text = text.replace(/ +/g, '-');
		return text;
	}

	private static getBaseUrl(): string {
		return process.env.NODE_ENV === 'production' ? '/' : 'http://localhost:8000/';
	}

	public clearCache() {
		this.cache.clear();
	}
}