/* eslint-disable */

export interface Message {
	msg: string;

	type: string;

	expires: number;
}

export default class FlashMessageService {

	public messages: Message[] = [];

	constructor() {
		// Delete old messages
		window.setInterval(() => {
			let now: number = new Date().getTime();
			for (let i = 0; i < this.messages.length; i++) {
				if (this.messages[i].expires < now) {
					this.messages.splice(i, 1);
				}
			}
		}, 500);
	}

	public ok(msg: string): void {
		this.messages.push({msg: msg, type: 'Success', expires: new Date().getTime() + 1000});
	}

	public error(msg: string): void {
		this.messages.push({msg: msg, type: 'Error', expires: new Date().getTime() + 1000});
	}

	public getMessages(): Message[] {
		return this.messages;
	}

	public warn(msg: string): void {
		this.messages.push({msg: msg, type: 'Warn', expires: new Date().getTime() + 1000});
	}
}