export default class DOMUtility {
	public static resetFocus() {
		if (document.activeElement && document.activeElement instanceof HTMLElement) document.activeElement.blur();
	}
}