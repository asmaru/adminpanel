/* eslint-disable */

import Resource from '@/models/Resource';
import ImageDimensionResponse from '@/models/ImageDimensionResponse';

export default class ResourceFactory {

	public static createEmptyResource(): Resource {
		return {
			dimensions: new ImageDimensionResponse(),
			changed: 0,
			_parent: '',
			_parentRelative: '',
			_self: '',
			_selfRelative: '',
			_preview: '',
			_public: '',
			children: [],
			data: {type: ''},
			name: '',
			type: '',
			size: 0
		};
	}
}