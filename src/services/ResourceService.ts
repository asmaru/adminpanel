export default class ResourceService {
	public static getInternalPath(path: string): string {
		return path + '.json';
	}

	public static getExternalPath(path: string): string {
		return path.replace('.json', '');
	}
}