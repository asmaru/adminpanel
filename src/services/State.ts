import ApiService from '@/services/ApiService';
import FlashMessageService from '@/services/FlashMessageService';

export default class State {

	api: ApiService = new ApiService();

	message: FlashMessageService = new FlashMessageService();
}